!
! Title: Focus filter
! Description: Filter to help kids focus on productive things :)

# Videos
/youtube/
/video/
/vimeo/
/twitch/

# Image editing
/pixilart/
/imgflip/
/pixel/
/piskel/

# Online play
/minecraft/
/hypixel/
/scratch/
/discord/

# Sharing sites rabbit holes
/imgur/
/reddit/

# Social media
/instagram/
/snapchat/
/twitter/

# Other unneccessary sites

/roblox/
/fortnite/
/roblox/
||netflix.com^
/game/

# Random play sites
/emulator/
/nate/
/lospec/
/soundcloud/
/commandgeek/
/calculator/
/merchforall/
/mcmakistein/
/pack/
/super/
/elgoog/
/rapid/
/yt1s/
/klopity/
/among/
/innersloth/
/gdevelop/
/forge/
/badlion/
/mediafire/
/autotip/
/pvp/
/archongames/

# Others

||playtube.pk^
||poki.com^
||yt1s.com^
||shellshock.io^
||crazygames.com^
||krunker.io^
||freegamesplay.net^
||sites.google.com/site/tyronesgameshack^
||youtubekids.com^

/sex/
/porn/
/gambling/
/gamble/

